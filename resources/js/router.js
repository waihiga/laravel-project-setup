/**
 * Created by douglas mburu on 07/09/2018.
 */
import Vue from 'vue';
import Router from 'vue-router'
Vue.use(Router);


export default new Router({
    routes: [
        {

        }
    ],
    mode: 'history',
    hashbang: false,
    history: true,
    linkActiveClass: 'active'
})
